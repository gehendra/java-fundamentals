package com.pluralsight.test.myapp;
import com.pluralsight.myapp.Flight;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertNotNull;
import static org.testng.AssertJUnit.assertNull;
import static org.testng.AssertJUnit.assertTrue;


/**
 * Created by gehendrakarmacharya on 7/3/16.
 */
public class FlightTest {

    @Test
    public void testSomething(){
        Flight flight = new Flight(1);
        assertNotNull(flight);

    }

    @Test
    public void testNull() {
        Flight flight = null;
        assertNull(flight);
    }
}

