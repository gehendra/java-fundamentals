package com.pluralsight.generics;
import java.util.Comparator;
/**
 * Created by gehendrakarmacharya on 7/4/16.
 */


public class AgeComparator implements Comparator<Person>{
    public int compare(Person left, Person right){
        return Integer.compare(left.getAge(), right.getAge());
    }
}
