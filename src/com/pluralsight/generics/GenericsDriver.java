package com.pluralsight.generics;
import java.util.*;
import java.util.Map;

import static java.lang.System.out;

/**
 * Created by gehendrakarmacharya on 7/2/16.
 */
public class GenericsDriver {
    public void run(){
        arrayIssue();
        listImplementation();
        setImplementation();
        mapImplementation();
    }

    public void arrayIssue(){
        Person don = new Person("Don Draper", 89);
        Person peggys = new Person("Peggy Olson", 65);

        Person[] madMen = { don, peggys };
        //out.println("here comes array issues...");
        //out.println(Arrays.toString(madMen));

        Person gehendra = new Person("Gehendra", 32);
        madMen = gehendra.add(madMen);

        Person bijay = new Person("Bijay", 40);
        madMen = bijay.add(madMen);
        //out.println(Arrays.toString(madMen));
    }

    public void listImplementation(){
        //List<Person> madMen;
        final float[] pts = new float[]{1,2,3,4 * 4};
        for (float f : pts) {
            out.println(Arrays.asList(f));
        }
        Person don = new Person("Don Draper", 89);
        Person peggys = new Person("Peggy Olson", 65);

        List<Person> madMen = new ArrayList<>();
        madMen.add(don);
        madMen.add(peggys);

        madMen.add(new Person("Gehendra", 32));

        // Using Iterator interface
        /*Iterator<Person> personIterator = madMen.iterator();
        while(personIterator.hasNext()){
            Person p = personIterator.next();
            out.println(p);
        }*/

        for(Person p : madMen){
            //out.println(p);
        }
        //out.println(madMen);
        // Implementation of collection sort with Comparator interface implementation
        //Collections.sort(madMen, new AgeComparator());

        madMen.sort(new AgeComparator()); // ArrayList collection implementing sort by age
        out.println(madMen); // Ascending order by age

        //Reverse comparator with using generic class with generic interface
        madMen.sort(new ReverseComparator<>(new AgeComparator())); // // Descending order by age
        out.println(madMen);

        final Person youngestCrewMember = min(madMen, new AgeComparator());
        out.println(youngestCrewMember);


        List<Integer> numbers = new ArrayList<>();
        numbers.add(1);
        numbers.add(2);
        numbers.add(3);

        final Integer lowestNumber = min(numbers, Integer::compare);

        out.println(lowestNumber);

    }

    public static <T> T min(List<T> values, Comparator<T> c){
        if (values.isEmpty()){
            throw new IllegalArgumentException("List is empty");
        }

        T lowestElement = values.get(0);
        for (int i = 1; i < values.size(); i++){
            final T element = values.get(i);
            if (c.compare(element, lowestElement) < 0)
                lowestElement = element;
        }
        return lowestElement;
    }

    public void setImplementation(){
        Person don = new Person("Don Draper", 89);
        Person peggys = new Person("Peggy Olson", 65);
        Set<Person> madMen = new HashSet<>();
        madMen.add(don);
        madMen.add(peggys);

        madMen.add(new Person("Gehendra", 32));

        madMen.add(don);

        /*out.println(madMen.contains(don));
        out.println(madMen.contains(peggys));
        out.println(madMen);*/
    }


    public void mapImplementation(){
        Person don = new Person("Don Draper", 89);
        Person peggys = new Person("Peggy Olson", 65);

        Map<String, Person> madMen = new HashMap<>();
        madMen.put(don.getName(), don);
        madMen.put(peggys.getName(), peggys);
        madMen.put("Gehendra", new Person("Gehendra", 32));

        // iterate over keys
        /*for(String name : madMen.keySet()){
            out.println(name);
        }*/

        // iterate over values which is person collection
        /*for(Person person : madMen.values()){
            out.println(person);
        }*/

        // iterate over key and values
        /*for(Map.Entry<String, Person> entry : madMen.entrySet()){
            out.println(entry.getKey());
            out.println(entry.getValue());
        }*/
    }
}
