package com.pluralsight.generics;


/**
 * Created by gehendrakarmacharya on 7/4/16.
 */
public class Partner extends Person {

    public Partner(final String name, final int age){
        super(name, age);
    };

    @Override
    public String toString(){
        return "Partner{" +
                "name=\'" + getName() + "\' " +
                "age: \'" + getAge() +
                "\'}";
    }
}
