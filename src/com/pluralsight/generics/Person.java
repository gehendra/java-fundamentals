package com.pluralsight.generics;

import java.util.Arrays;
import java.util.Objects;

/**
 * Created by gehendrakarmacharya on 7/3/16.
 */
public class Person {
    private final String name;
    private final int age;

    public Person(String name, int age){
        Objects.requireNonNull(name);
        this.name = name;
        this.age = age;
    }

    public int getAge(){
        return age;
    }

    public String getName(){
        return name;
    }

    @Override
    public boolean equals(Object o){
        if (o == null || getClass() != o.getClass())
            return false;
        Person p = (Person) o;

        return age == p.age && name.equals(p.name);
    }

    @Override
    public int hashCode(){
        int result = name.hashCode();
        result = 31 * result + age;
        return result;
    }

    @Override
    public String toString(){
        return "Person{" +
                "name=\'" + name + "\' " +
                "age: \'" + age +
                "\'}";
    }

    public Person[] add(Person[] persons){
        int length = persons.length;
        Person[] newArrayPerson = Arrays.copyOf(persons, length + 1);
        newArrayPerson[length] = this;
        return newArrayPerson;
    }
}
