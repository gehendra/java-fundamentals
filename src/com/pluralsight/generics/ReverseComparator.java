package com.pluralsight.generics;

import com.sun.org.apache.regexp.internal.RE;

import java.util.Comparator;

/**
 * Created by gehendrakarmacharya on 7/4/16.
 */
public class ReverseComparator<T> implements Comparator<T> {
    final private Comparator<T> delegateComparator;
    public ReverseComparator(Comparator<T> delegateComparator){
        this.delegateComparator = delegateComparator;
    }
    public int compare(T left, T right){
        return -1 * delegateComparator.compare(left, right);
    }
}