package com.pluralsight.myapp;

/**
 * Created by gehendrakarmacharya on 7/17/16.
 */
public class TemperatureDriver {
    public void run(){
        int x = 5;
        int y = x;
        x = 7;

        System.out.println(x + " " + y);

        Temperature t1 = new Temperature(24);

        Temperature t2 = t1;

        Temperature t3 = new Temperature(24);
        Temperature t4 = new Temperature(37);

        System.out.println(t1);
        System.out.println(t2);
        System.out.println(t3);
        System.out.println(t4);

        // identity
        System.out.println(t1 == t1 ? "t1 == t1" : "t1 != t1");
        System.out.println(t1 == t2 ? "t1 == t2" : "t1 != t2");
        System.out.println(t1 == t3 ? "t1 == t3" : "t1 != t3");

        // equality
        System.out.println(t1.equals(t1) ? "t1.equals(t1)" : "! t1.equals(t1)");
        System.out.println(t1.equals(t2) ? "t1.equals(t2)" : "! t1.equals(t2)");
        System.out.println(t1.equals(t3) ? "t1.equals(t3)" : "! t1.equals(t3)");
        System.out.println(t1.equals(null) ? "t1.equals(null)" : "! t1.equals(null)");

        // cloning
        Temperature t5 = t1.clone();
        System.out.println(t5);
        System.out.println(t1 == t5 ? "t1 == t5" : "t1 != t5");
        System.out.println(t1.equals(t5) ? "t1.equals(t5)" : "! t1.equals(t5)");

    }
}
