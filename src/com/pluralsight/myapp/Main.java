package com.pluralsight.myapp;

import com.pluralsight.adapter.AdapterDriver;
import com.pluralsight.generics.GenericsDriver;
import com.pluralsight.getorganized.Driver;

import java.awt.*;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.*;

public class Main {

    public static void main(String[] args) {

        // declaring allocate space reference object want to use
        // assign

        Driver driver = new Driver();
        //driver.flightDriver();

        //driver.mathDriver();

        //driver.flightParameterDriver();

        //driver.mathCalculationInheritanceDriver();


        //driver.enumDriver();


        //driver.calcualteHelperDriver();
        //System.out.println("###  Calculate Driver Ends Here  ###");
        //driver.readFileDriver();


        //driver.checkComparable();

        /* User of interface comparable, interable and Iterator */

        /*CrewMember crew1 = new CrewMember("crew 1");
        CrewMember crew2 = new CrewMember("crew 2");
        CrewMember crew3 = new CrewMember("crew 3");
        CrewMember crew4 = new CrewMember("crew 4");
        CrewMember crew5 = new CrewMember("crew 5");

        CrewMember[] crewMembers = { crew1, crew2, crew3, crew4, crew5 };


        Passenger p1 = new Passenger("p 1");
        Passenger p2 = new Passenger("p 2");
        Passenger p3 = new Passenger("p 3");
        Passenger p4 = new Passenger("p 4");
        Passenger p5 = new Passenger("p 5");

        Passenger[] passengers = { p1, p2, p3, p4, p5 };

        Flight flight = new Flight(45, crewMembers, passengers);

        for(Person p: flight){
            System.out.println(p.getName());
        }


        // Use of nested class
        Passenger gehendra = new Passenger();
        gehendra.getRewardProgram();

        Passenger.RewardProgram gehendra1 = new Passenger.RewardProgram();*/



        // Fundamental session starts from here

        //driver.fundamentalsDriver();

        // Java Fundamentals: Generics Richard Warburton starts here
        GenericsDriver genericsDriver = new GenericsDriver();
        //genericsDriver.run();


        // Pattern starts here
        // Adapter pattern
        AdapterDriver adapterDriver = new AdapterDriver();
        //adapterDriver.run();


        //TemperatureDriver temperatureDriver = new TemperatureDriver();
        //temperatureDriver.run();


        abstract class C implements I { public int f() { return 3; } }
        class D extends C {
            public int f() { return 7; }
            public int g() { return f() + 5 * super.f(); }
        }
        class E extends D { public int f() { return 11; } }
        class F extends E { public int f() { return f(); } }
//... in some other class, perhaps in main() ...
        final I x = new D(); System.out.println(x.g());
        final I y = new E(); System.out.println(y.g());

        final I z = new F();
        System.out.println(z.f());




    }

    interface I { int f(); int g(); }
}


