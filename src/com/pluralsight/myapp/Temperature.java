package com.pluralsight.myapp;

/**
 * Created by gehendrakarmacharya on 7/17/16.
 */
public class Temperature{
    private  int t;

    public  Temperature(final int t){
        this.t = t;
    }

    public void set(final int  t){
        this.t = t;
    }

    public int get(){
        return t;
    }

    @Override
    public String toString(){
        return super.toString() + "[t=" + t + "]";
    }

    @Override
    public boolean equals(final Object that){
        return this == that || that instanceof Temperature && this.get() == ((Temperature) that).get();
    }

    @Override
    public int hashCode(){
        return get();
    }

    @Override
    public Temperature clone(){
        try{
            Temperature result = (Temperature) super.clone();
            return result;
        } catch (CloneNotSupportedException ex){
            throw new RuntimeException("Something went really wrong", ex);
        }
    }
}
