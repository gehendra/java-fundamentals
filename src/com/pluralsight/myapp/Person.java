package com.pluralsight.myapp;

/**
 * Created by gehendrakarmacharya on 6/28/16.
 */
public class Person {
    private String name;

    public  Person(){};

    public Person(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
}
