package com.pluralsight.myapp;

/**
 * Created by gehendrakarmacharya on 6/28/16.
 */
public class Passenger extends Person{
    public Passenger(String name){
        super(name);
    }
    public Passenger(){}


    // nesting class
    public static class RewardProgram {
        private int memberLevel;
        private int memberDays;

        public int getMemberDays() {
            return memberDays;
        }

        public void setMemberDays(int memberDays) {
            this.memberDays = memberDays;
        }

        public int getMemberLevel() {
            return memberLevel;
        }

        public void setMemberLevel(int memberLevel) {
            this.memberLevel = memberLevel;
        }
    }

    private RewardProgram rewardProgram = new RewardProgram();

    public RewardProgram getRewardProgram() {
        return rewardProgram;
    }
}
