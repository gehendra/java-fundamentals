package com.pluralsight.myapp;

import java.util.Iterator;

/**
 * Created by gehendrakarmacharya on 6/28/16.
 */
public class Flight implements  Comparable<Flight>, Iterable<Person>{
    private  int flightTime;

    private CrewMember[] crew;
    private Passenger[] roster;

    public Flight(int flightTime){
        this.flightTime = flightTime;
    }

    public Flight(int flightTime, CrewMember[] crew, Passenger[] roster){
        this(flightTime);
        this.crew = crew;
        this.roster = roster;
    }

    public int compareTo(Flight f){
        return this.flightTime - f.flightTime;
    }

    public Iterator<Person> iterator(){
        return new FlightIterator();
    }

    // Inner class as it doesn't have static keyword in the class
    private class FlightIterator implements Iterator<Person> {
        private int index = 0;

        public boolean hasNext(){
            return index < (crew.length + roster.length);
        }

        public Person next(){
            Person p = index < crew.length ? crew[index] : roster[index - crew.length];
            index++;
            return p;
        }
    }
}
