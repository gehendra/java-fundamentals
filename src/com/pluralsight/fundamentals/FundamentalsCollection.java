package com.pluralsight.fundamentals;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import static java.lang.System.out;

/**
 * Created by gehendrakarmacharya on 7/2/16.
 */
public class FundamentalsCollection {
    public void collectionsCollection(){
        //basicCollection();
        collectionList();
    }

    public void basicCollection(){
        Product computer = new Product("MAC", 5);
        Product keyboard = new Product("Smart Keyboard", 3);
        Product iphone = new Product("iPhone", 2);
        Product soccer = new Product("Soccer", 10);

        Collection<Product> products = new ArrayList<>();
        products.add(computer);
        products.add(keyboard);
        products.add(iphone);
        products.add(soccer);


        Iterator<Product> productIterator = products.iterator();

        while(productIterator.hasNext()){
            Product product = productIterator.next();
            if (product.getWeight() > 3)
                out.println(product);
            else
                productIterator.remove();
        }

        out.println("After removed..." + products);
        out.println(products.contains(keyboard));

        /*for (Product product : products){
            out.println(product);
        }*/
    }

    public void collectionList(){
        // order, indexing

        Product computer = new Product("MAC", 5);
        Product keyboard = new Product("Smart Keyboard", 3);
        Product iphone = new Product("iPhone", 2);
        Product soccer = new Product("Soccer", 10);

        List<Product> products = new ArrayList<>();

        products.add(0, computer);
        products.add(1,keyboard);
        products.add(2,iphone);
        products.add(3,soccer);

        products.remove(3);
        products.add(iphone);
        products.add(iphone);
        out.println(products.lastIndexOf(iphone));

        out.print(products.subList(1,3));
    }

    public void collectionSet(){
        /*
        * sets collection with distinct elements
        * sortedsets sorted order
        * */
    }

    public void collectionQueue(){
        /*
        * FIFO
        *
        *
        * Deque => doule ended queue
        *
        * */
    }


    public void collectionDeQueue(){

    }
}
