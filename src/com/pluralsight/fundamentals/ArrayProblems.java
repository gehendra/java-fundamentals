package com.pluralsight.fundamentals;
import static java.lang.System.out;

import java.util.Arrays;

/**
 * Created by gehendrakarmacharya on 7/2/16.
 */
public class ArrayProblems {
    ArrayProblems(){};

    public void arrayTest(){
        arrayAddOpeartion();
    }

    public void arrayAddOpeartion(){
        // Array add operation issue
        Product computer = new Product("MAC", 5);
        Product keyboard = new Product("Smart Keyboard", 3);

        // Products array
        Product[] products = { computer, keyboard };
        out.println("Original" + Arrays.toString(products)); // toString overwritten to display product in friendly way

        // Adding new product to an existing array
        Product newProduct = new Product("iPhone", 2);
        products = arrayAdd(newProduct, products); // custom array to add new product to existing array
        out.println("After adding new product to the array" + Arrays.toString(products));
    }

    private Product[] arrayAdd(Product product, Product[] productArray){
        int productArrayLength = productArray.length;
        Product[] newProductArray = Arrays.copyOf(productArray, productArrayLength + 1);
        newProductArray[productArrayLength] = product;
        return newProductArray;
    }
}
