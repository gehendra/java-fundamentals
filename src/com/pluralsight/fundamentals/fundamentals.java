package com.pluralsight.fundamentals;

/**
 * Created by gehendrakarmacharya on 7/1/16.
 */
public class Fundamentals {
    Fundamentals(){};

    void run(){
        System.out.println("Java Fundamentals: Collections starts here...");
        //runArray();
        runCollection();
    }

    void runArray(){
        ArrayProblems arrayProblems = new ArrayProblems();
        arrayProblems.arrayTest();
    }

    void runCollection(){
        //collection methods goes here
        FundamentalsCollection fCollection = new FundamentalsCollection();
        fCollection.collectionsCollection();
    }
}
