package com.pluralsight.getorganized;

/**
 * Created by gehendrakarmacharya on 6/28/16.
 */
public class Member implements Comparable{
    private int memberLevel;
    private int memberDays;
    private String name;

    void setLevelandDays(int memberLevel, int memberDays, String name){
        this.memberLevel = memberLevel;
        this.memberDays = memberDays;
        this.name = name;
    }

    String getName(){
        return this.name;
    }

    int getMemberLevel(){
        return this.memberLevel;
    }

    int getMemberDays(){
        return  this.memberDays;
    }

    public int compareTo(Object o){
        Member p = (Member) o;

        if (this.memberLevel > p.memberLevel)
            return -1;
        else if (this.memberLevel < p.memberLevel)
            return 1;
        else {
            if (this.memberDays > p.memberDays)
                return -1;
            else if (this.memberDays < p.memberDays)
                return 1;
            else
                return 0;
        }
    }
}
