package com.pluralsight.getorganized;

/**
 * Created by gehendrakarmacharya on 6/28/16.
 */
public class InvalidStatementException extends Exception{
    public InvalidStatementException(String reason, String statement){
        super(reason + ": " + statement );
    }

    public InvalidStatementException(String reason, String statement, Throwable cause){
        super(reason + ": " + statement, cause);
    }
}
