package com.pluralsight.getorganized;

/**
 * Created by gehendrakarmacharya on 6/21/16.
 */
enum MathCommand {
    Add,
    Subtract,
    Multiply,
    Divide
}