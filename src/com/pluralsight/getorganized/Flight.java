package com.pluralsight.getorganized;

/**
 * Created by Gehendra Karmacharya on 6/18/16.
 */
public class Flight {
    private int passengers;
    private  int seats;
    private int maximumAllowedBags = 2 * seats,totalCheckingBags;

    public Flight() {
        getSeats();
        passengers = 0;
    }

    int getSeats() {
        return 150;
    }

    public int getPassengers(){
        return passengers;
    }

    public boolean hasRoom(Flight f2){
        int total = passengers + f2.passengers;
        return total <= seats;
    }

    public Flight createNewFlight(Flight f2){
        Flight newFlight = new Flight();
        newFlight.seats = this.seats;
        newFlight.passengers = this.passengers + f2.passengers;
        return newFlight;
    }

    public void add1Passenger(){
        if (hasSeating())
            passengers += 1;
        else
            handleTooMany();
    }

    public  void add1Passenger(int bags) {
        if(hasSeating()) {
            add1Passenger();
            totalCheckingBags += bags;
        }
    }

    public void add1Passenger(Passenger p) {
        add1Passenger(p.getCheckedBags());
    }

    /*public void addPassengers(Passenger[] list) {
        if(hasSeating(list.length)) {
            for(Passenger passenger : list){
                totalCheckingBags += passenger.getCheckedBags();
            }
        }
        else
            handleTooMany();
    }*/

    public void addPassengers(Passenger... list) {
        if(hasSeating(list.length)) {
            for(Passenger passenger : list){
                totalCheckingBags += passenger.getCheckedBags();
            }
        }
        else
            handleTooMany();
    }

    private  boolean hasSeating() {
        return passengers < getSeats();
    }

    private boolean hasSeating(int count) {
        return passengers + count <= getSeats();
    }

    private void handleTooMany(){
        System.out.println("Too many passengers");
    }
}