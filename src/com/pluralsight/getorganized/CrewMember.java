package com.pluralsight.getorganized;

/**
 * Created by gehendrakarmacharya on 6/21/16.
 */
public class CrewMember {
    private FlightCrewJob job;

    CrewMember(FlightCrewJob job){
        this.job = job;
    }

    public void setJob(FlightCrewJob job){
        this.job = job;
    }

    public String getJob(){
        return this.job.toString();
    }
}
