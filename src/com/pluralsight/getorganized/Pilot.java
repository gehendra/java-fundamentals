package com.pluralsight.getorganized;

/**
 * Created by gehendrakarmacharya on 6/19/16.
 */
public abstract class Pilot {
    private Flight currentFlight;

    public void fly(Flight f) {
        if (canAccept(f)){
            currentFlight = f;
        }
        else
            handleCantAccept();
    }

    public abstract boolean canAccept(Flight F);

    private void handleCantAccept() {
        System.out.println("Can't accept");
    }
}
