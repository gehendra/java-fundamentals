package com.pluralsight.getorganized;

/**
 * Created by gehendrakarmacharya on 6/21/16.
 */
enum FlightCrewJob {
    Pilot,
    CoPilot,
    FlightAttendent,
    AirMarshal
}
