package com.pluralsight.getorganized;

/**
 * Created by gehendrakarmacharya on 6/28/16.
 */
public class PowerOF implements  MathProcessing{

    @Override
    public String getKeyword() {
        return "power";
    }

    @Override
    public char getSymbol() {
        return '^';
    }

    @Override
    public double doCalculate(double leftVal, double rightVal) {
        return Math.pow(leftVal, rightVal);
    }
}
