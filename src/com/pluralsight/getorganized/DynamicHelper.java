package com.pluralsight.getorganized;

/**
 * Created by gehendrakarmacharya on 6/28/16.
 */
public class DynamicHelper {
    private MathProcessing[] handlers;

    public DynamicHelper(MathProcessing[] handlers){
        this.handlers = handlers;
    }

    public String process(String statement){
        String[] parts = statement.split(MathProcessing.SEPERATOR);
        String keyword = parts[0];


        MathProcessing theHandler = null;

        for(MathProcessing handler : handlers){
            if(keyword.equalsIgnoreCase(handler.getKeyword())){
                theHandler = handler;
                break;
            }
        }

        double leftValue = Double.parseDouble(parts[1]);
        double rightValue = Double.parseDouble(parts[2]);

        double result = theHandler.doCalculate(leftValue, rightValue);

        StringBuilder sb = new StringBuilder(20);
        sb.append(leftValue);
        sb.append(' ');
        sb.append(theHandler.getSymbol());
        sb.append(' ');
        sb.append(rightValue);
        sb.append(" = ");
        sb.append(result);

        return sb.toString();
    }
}
