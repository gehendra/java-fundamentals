package com.pluralsight.getorganized;

/**
 * Created by gehendrakarmacharya on 6/19/16.
 */
public class Adder extends CalculateBase implements MathProcessing{

    public Adder() {};

    public Adder(double leftVal, double rightVal) {
        super(leftVal, rightVal);
    }

    @Override
    public void calculate(){
        double total = getLeftVal() + getRightVal();
        setResult(total);
    }

    @Override
    public String getKeyword() {
        return "add";
    }

    @Override
    public char getSymbol() {
        return '+';
    }

    @Override
    public double doCalculate(double leftVal, double rightVal) {
        setLeftVal(leftVal);
        setRightVal(rightVal);
        calculate();
        return getResult();
    }
}
