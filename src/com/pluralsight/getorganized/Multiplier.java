package com.pluralsight.getorganized;

/**
 * Created by gehendrakarmacharya on 6/19/16.
 */
public class Multiplier extends CalculateBase{

    public Multiplier(){};

    public Multiplier(double leftVal, double rightVal){
        super(leftVal, rightVal);
    }

    @Override
    public void calculate(){
        double value = getLeftVal() * getRightVal();
        setResult(value);
    }
}
