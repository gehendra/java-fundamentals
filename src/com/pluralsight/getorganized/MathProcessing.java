package com.pluralsight.getorganized;

/**
 * Created by gehendrakarmacharya on 6/28/16.
 */
public interface MathProcessing {
    String SEPERATOR = " ";
    String getKeyword(); // add
    char getSymbol();
    double doCalculate(double leftVal, double rightVal);
}
