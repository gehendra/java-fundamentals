package com.pluralsight.getorganized;
import com.pluralsight.fundamentals.FundamentalsDriver;

import java.io.*;
import java.util.Arrays;

/**
 * Created by gehendrakarmacharya on 6/19/16.
 */
public class Driver {

    public void enumDriver(){
        StringPractice stringPractice = new StringPractice();
        stringPractice.stringTest();
        CrewMember gehendra = new CrewMember(FlightCrewJob.CoPilot);
        String getJob = gehendra.getJob();
        System.out.println(getJob);
        gehendra.setJob(FlightCrewJob.Pilot);
        getJob = gehendra.getJob();
        System.out.println(getJob);
    }

    public void flightDriver(){
        Flight myFlight1; // allocate space to store reference to the object want to use
        myFlight1 = new Flight(); // allocate memory associate to the class and return it to the reference.

        myFlight1.add1Passenger();
        myFlight1.add1Passenger();
        myFlight1.add1Passenger();
        myFlight1.add1Passenger();
        myFlight1.add1Passenger();
        myFlight1.add1Passenger();

        System.out.println("Total number of passengers: " + myFlight1.getPassengers());

        Flight myFlight2 = new Flight();
        myFlight2.add1Passenger();
        myFlight2.add1Passenger();
        myFlight2.add1Passenger();
        myFlight2.add1Passenger();
        myFlight2.add1Passenger();

        System.out.println("Total number of passengers: " + myFlight2.getPassengers());

        Flight myFlight3 = null;

        if (myFlight1.hasRoom(myFlight2)) {
            myFlight3 = myFlight1.createNewFlight(myFlight2);
            System.out.println("Total passengers " + myFlight3.getPassengers());
        }
    }

    public void mathDriver(){
        // Initial without proper class implementation
        //MathCalculation[] cal = new MathCalculation();
        //cal.calculation();

        // Math Calculator
        MathCalculation[] cal = new MathCalculation[4];
        cal[0] = new MathCalculation('d', 100.0d, 50.0d);
        cal[1] = new MathCalculation('a', 25.0d, 92.0d);
        cal[2] = new MathCalculation('s', 225.0d, 17.0d);
        cal[3] = new MathCalculation('m', 11.0d, 3.0d);

        for (MathCalculation eq : cal) {
            eq.execute();
            System.out.println("The Result: " + eq.getResult());
        }

        double doubleLeftVal = 10.0d;
        double doubleRightVal = 4.0d;
        MathCalculation mathCalc = new MathCalculation('d');
        mathCalc.execute(doubleLeftVal, doubleRightVal);
        System.out.println("Overload: " + mathCalc.getResult());


        int intLeftVal = 10;
        int intRightVal = 4;
        MathCalculation intMathCalc = new MathCalculation('d');
        mathCalc.execute(intLeftVal, intRightVal);
        System.out.println("Integer Overload: " + mathCalc.getResult());
    }

    public void flightParameterDriver(){
        Flight f = new Flight();

        Passenger bijay = new Passenger(1);
        Passenger gehendra = new Passenger(1);

        Passenger[] arrPassenger = new Passenger[]{gehendra, bijay};

        System.out.println("Total number of passenger " + arrPassenger.length);

        f.addPassengers(arrPassenger);

        f.addPassengers(new Passenger[]{gehendra, bijay});


        Passenger passenger1 = new Passenger(0,2);
        Passenger passenger2 = new Passenger(1,3);
        Passenger passenger3 = new Passenger(4,1);

        f.addPassengers(passenger1, passenger2, passenger3);


        CargoFlight cf = new CargoFlight();
        cf.add1Package(1.0f, 2.5f, 3.0f);

        cf.add1Passenger(bijay);

        Flight flight = new Flight();
        System.out.println("Flight seats: " + flight.getSeats());

        CargoFlight cargoFlight = new CargoFlight();
        System.out.println("Cargo Flight seats: " + cargoFlight.getSeats());

        Flight cargoFlightFlight = new CargoFlight();
        System.out.println("Cargo Flight Flight seats: " + cargoFlightFlight.getSeats());

        Object o = new CargoFlight();
        if (o instanceof CargoFlight) {
            System.out.println("It is an instance of cargo flight");
            CargoFlight cf1 = (CargoFlight) o;
            cf1.add1Package(1.0f, 2.5f, 3.0f);
        }
    }

    public void mathCalculationInheritanceDriver(){
        System.out.println();
        System.out.println("Entering inheritence from here...");


        CalculateBase[] calculators = {
                new Divider(100.0d, 50.0d),
                new Adder(25.0d, 92.0d),
                new Subtractor(225.0d, 17.0d),
                new Multiplier(11.0d, 3.0d)
        };

        for (CalculateBase calculator : calculators){
            calculator.calculate();
            System.out.println("Results: " + calculator.getResult());
        }
        System.out.println();
    }

    public void calcualteHelperDriver(){

        String[] statements = {
            "add 25.0 92.0",
            "power 5.0 2.0"
        };

        DynamicHelper dhelper = new DynamicHelper(new MathProcessing[] {
                new Adder(),
                new PowerOF()
        });

        for (String statement : statements){
            String output = dhelper.process(statement);
            System.out.println(output);
        }


        /* Old Math Cal with inheritence */
        System.out.println("###  Math Calculation with inheritence ###");
        String[] statments = {
                "divide 100.50 50.0",
                "add 25.0 92.0",
                "subtract 225.00 17.0",
                "multiply 11.0 3.0"
        };

        CalculateHelper helper = new CalculateHelper();
        for (String statement : statments){
            try{
                helper.process(statement);
                System.out.println(helper.toString());
            } catch (InvalidStatementException e){
                System.out.println(e.getMessage());
                if (e.getCause() != null){
                    System.out.println("Original message" + e.getCause().getMessage());
                }
            }
        }
    }

    public void readFileDriver() {
        BufferedReader reader = null;

        int total = 0;

        try {
            reader = new BufferedReader(new FileReader("//Users//gehendrakarmacharya//projects//motivation//letsdosomething//pluralsight//funstuffs//java//java_fundamentails_jim_wilson//intellij//Oranized//Numbers.txt"));
            String line = null;
            while ((line = reader.readLine()) != null) {
                total += Integer.valueOf(line);
            }
            System.out.println(total);
        } catch (NumberFormatException e) {
            System.out.println("Invalid value: " + e.getMessage());
        } catch (FileNotFoundException e) {
            System.out.println("Not found: " + e.getMessage());
        } catch (IOException e){

        } finally{
            try {
                if (reader != null)
                    reader.close();
            } catch(Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }



    public void checkComparable(){
        Member gehendra = new Member();
        gehendra.setLevelandDays(1, 180, "gehendra");

        Member bijay = new Member();
        bijay.setLevelandDays(1, 40, "bijay");

        Member jordan = new Member();
        jordan.setLevelandDays(2, 180, "jordan");

        Member chicago = new Member();
        chicago.setLevelandDays(3, 10, "chicago");

        Member[] passengers = {gehendra, bijay, jordan, chicago};

        Arrays.sort(passengers);

        for (Member p : passengers){
            System.out.println(p.getName() + " " + p.getMemberLevel() + " " + p.getMemberDays());
        }
    }



    public void fundamentalsDriver(){
        FundamentalsDriver fundamentalsDriver = new FundamentalsDriver();
        fundamentalsDriver.funDriver();
    }
}