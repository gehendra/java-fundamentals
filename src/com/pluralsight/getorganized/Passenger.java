package com.pluralsight.getorganized;

/**
 * Created by gehendrakarmacharya on 6/18/16.
 */
public class Passenger {

    private int freeBags;
    private  int checkedBags;
    private double perBagFree;

    public Passenger() {

    }

    public Passenger(int freeBags) {
        this(freeBags > 1 ? 25.0d : 50.0d);
        this.freeBags = freeBags;
    }

    public Passenger(int freeBags, int checkedBags) {
        this(freeBags);
        this.checkedBags = checkedBags;
    }

    public int getCheckedBags() {
        return checkedBags;
    }

    public void setCheckedBags(int checkedBags) {
        this.checkedBags = checkedBags;
    }

    private Passenger(double perBagFree) {
        this.perBagFree = perBagFree;
    }
}
